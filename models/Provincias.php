<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "provincias".
 *
 * @property string $autonomia
 * @property string $provincia
 * @property int $poblacion
 * @property int $superficie
 */
class Provincias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provincias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['provincia'], 'required'],
            [['poblacion', 'superficie'], 'integer'],
            [['autonomia', 'provincia'], 'string', 'max' => 255],
            [['provincia'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'autonomia' => 'Autonomías',
            'provincia' => 'Provincias',
            'poblacion' => 'Población',
            'superficie' => 'Superficie',
        ];
    }
}
